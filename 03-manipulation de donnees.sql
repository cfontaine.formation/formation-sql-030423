/* 03-manipulation de données*/

USE exemple;
-- Ajouter des données => INSERT 
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO marques VALUES (1,'Marque A','1980-03-03');
INSERT INTO marques VALUES (2,'Marque B','2000-01-02');

-- Insérer une ligne en spécifiant les colonnes
INSERT INTO fournisseurs(nom) VALUES ('fournisseur 1');
INSERT INTO marques(nom,date_creation) VALUES ('Marque C','1950-12-04');
INSERT INTO marques(nom) VALUES ('Marque D');

-- Insérer plusieurs lignes à la fois
INSERT INTO articles(reference,description,prix_unitaire,id_marque)
VALUES (1,'TV 4K',600.00,2),
(2,'Pc portable',850.0,1),
(3,'Souris',15.0,1);

-- Erreur => La marque avec l'id 10 n'existe pas, la contrainte d'intégrité référentielle, n'est pas respecté
-- INSERT INTO articles(reference,description,prix_unitaire,id_marque) VALUES (4,'Erreur contrainte clé étrangère ',600.00,10);

INSERT INTO fournisseurs(nom) VALUES ('fournisseur 2');

INSERT INTO articles_fournisseurs (reference_article,id_fournisseur)
VALUES(1,1),
      (3,1),
      (1,2),
      (2,2);
     
-- Supprimer des données => DELETE, TRUNCATE
-- Supression de la ligne qui a pour reference 4 dans la table marques
DELETE FROM marques WHERE id=4;   

INSERT INTO marques (nom,date_creation) VALUES ('Marque E','2010-01-02')
,('Marque F','2012-01-02')
,('Marque G','2015-01-02');

-- Supprimer toutes les lignes qui ont une date de création > 01/01/2005
DELETE FROM marques WHERE date_creation > '2005-01-01';

-- DELETE => ne ré-initialise pas la colonne AUTO_INCREMENT
INSERT INTO pilotes(prenom, nom, nombre_heure_vol) VALUES 
('John','DOE',145),
('Jane','DOE',200),
('Marcel','Dupond',250);

-- Supression de toutes les lignes de la table articles
DELETE FROM pilotes;

INSERT INTO pilotes(prenom, nom, nombre_heure_vol) VALUES 
('John','DOE',145),
('Jane','DOE',200),
('Marcel','Dupond',250);

SET FOREIGN_KEY_CHECKS=0;  -- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table avions  => remet les colonnes AUTO_INCREMENT à 0
TRUNCATE TABLE pilotes;
SET FOREIGN_KEY_CHECKS=1; --réactiver la vérification des clés étrangères

INSERT INTO pilotes(prenom, nom, nombre_heure_vol) VALUES 
('John','DOE',145),
('Jane','DOE',200),
('Marcel','Dupond',250);

-- Modification des données => UPDATE
-- Modification de la description de l'article qui a pour reference 1
UPDATE articles SET description = 'TV HD',prix_unitaire=350 WHERE reference=1;

-- Modification des article dont le prix est inférieur à 100 -> 20
UPDATE articles SET prix_unitaire=20 WHERE prix_unitaire <100;

-- Modifier tous les prix en les augmentant de 10%
UPDATE articles SET prix_unitaire=prix_unitaire *1.10;

-- Chargement d'un fichier binaire => LOAD_FILE
USE pizzeria;
INSERT INTO pizzas (nom,base,photo) VALUES('pizza aux 4 fromages','rouge',LOAD_FILE('C:/Dawan/pizza4.jpg'));

-- Chargement d'un fichier CSV
LOAD DATA INFILE 'c:/Dawan/ingredient.csv'
INTO TABLE ingredients
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;



USE exemple;

-- Cascade 
-- On supprime la contrainte  fk_articles_marques
ALTER TABLE articles
DROP CONSTRAINT fk_articles_marques;

-- On recrée la contrainte fk_articles_marques
-- en ajoutant une cascade pour la suppression et  la mise à jour
ALTER TABLE articles
ADD CONSTRAINT fk_articles_marques 
FOREIGN KEY (id_marque)
REFERENCES marques(id)
ON DELETE CASCADE,
ON UPDATE CASCADE ;

-- Progation de la supression
DELETE FROM marques WHERE id=2; 

-- Progation de la mise à jour
UPDATE marques SET id=9 WHERE id=1;
UPDATE articles SET id_marque=8 WHERE id_marque=1
