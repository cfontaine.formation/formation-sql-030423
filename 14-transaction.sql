USE exemple;

CREATE TABLE compte_bancaire(
	id INT PRIMARY KEY AUTO_INCREMENT,
	solde DECIMAL(8,3),
	isbn VARCHAR(20),
	prenom_titulaire VARCHAR(40),
	nom_titulaire VARCHAR(40)
);

INSERT INTO compte_bancaire (solde,isbn,prenom_titulaire,nom_titulaire)
VALUES(400,'5962-0000-0001','John','Doe'),
(1400,'5962-0000-0010','Jane','Doe'),
(500,'5962-0000-00012','Alan','Smithee'),
(1100,'5962-0000-00031','Yves','Roulot');

SET autocommit =0;
-- transfert
START TRANSACTION; # ouverture de la transaction
UPDATE compte_bancaire SET solde=solde-200.00 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+200.00 WHERE id=3;
COMMIT; # validation des requête de la transaction

SELECT * FROM compte_bancaire ;

START TRANSACTION;
UPDATE compte_bancaire SET solde=solde-50.00 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+50.00 WHERE id=2;
ROLLBACK;# on annule les requêtes de la transaction

SELECT * FROM compte_bancaire ;

START TRANSACTION; # ouverture de la TRANSACTION
SAVEPOINT sp_transfert1 ;
UPDATE compte_bancaire SET solde=solde-200.00 WHERE id=4;
UPDATE compte_bancaire SET solde=solde+200.00 WHERE id=2;
RELEASE SAVEPOINT sp_transfert1 ;

SAVEPOINT sp_createcompte ;
INSERT INTO compte_bancaire (solde,isbn,prenom_titulaire,nom_titulaire)
VALUES(4000,'5962-0000-0051','Jo','Dalton');
ROLLBACK TO sp_createcompte;

SAVEPOINT sp_transfert2;
UPDATE compte_bancaire SET solde=solde-200.00 WHERE id=3;
UPDATE compte_bancaire SET solde=solde+200.00 WHERE id=1;
RELEASE SAVEPOINT sp_transfert2;
COMMIT; # validation des requête de la transaction

SELECT * FROM compte_bancaire ;

SET autocommit =1;