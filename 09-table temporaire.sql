USE exemple;

-- Table temporaire
CREATE TEMPORARY TABLE utilisateurs
(
	id INT  PRIMARY KEY AUTO_INCREMENT,
	login VARCHAR(40),
	password VARCHAR(40)
);

INSERT INTO  utilisateurs(login,password) VALUES
('johndoe','azerty'),
('janedoe','123456'),
('yvesroulot','123456');

SELECT * FROM utilisateurs ; 

-- Table CTE
USE bibliotheque;

-- Declaration de la table CTE 
WITH auteur_vivant_cte AS(
	SELECT id,prenom,nom,naissance,nation FROM auteurs  WHERE deces IS NULL
)
-- SELECT * FROM livres;
SELECT prenom,nom FROM auteur_vivant_cte WHERE naissance > '1955-01-01';


-- Declaration de la table CTE multiple
WITH auteur_vivant_cte AS(
	SELECT auteurs.id,auteurs.prenom,auteurs.nom,auteurs.naissance,auteurs.nation
	FROM auteurs  WHERE deces IS NULL
),
auteur_vivant_usa_cte AS(
	SELECT auteur_vivant_cte.id,auteur_vivant_cte.prenom,auteur_vivant_cte.nom,auteur_vivant_cte.naissance FROM auteur_vivant_cte
	INNER JOIN pays ON pays.id= auteur_vivant_cte.nation
	WHERE pays.nom ='États-Unis'
)
-- SELECT * FROM livres;
SELECT prenom,nom FROM auteur_vivant_usa_cte WHERE naissance < '1950-01-01';

-- Création d'une table à partir d'un select
CREATE TEMPORARY TABLE auteurpost50 SELECT id,prenom,nom
FROM auteurs
WHERE naissance <'1950-01-01';

