/* 04-Ecrire des requêtes */

-- Sélectionner la base des données bibliotheque
USE bibliotheque;

SELECT prenom, nom FROM auteurs;

-- * -> Obtenir toutes les colonnes d'une table
SELECT * FROM auteurs;

-- Quand il ya une ambiguitée sur le nom d'une colonne, on ajoute le nom de la table => nom_table.nom_colonne
SELECT prenom, auteurs.nom , pays.nom FROM auteurs,pays;

-- On peut mettre dans les colonnes d'un SELECT une constante ou une colonne qui provient d'un calcul
SELECT titre,'age=',2023-annee FROM livres;

-- ALias 
-- un alias permet de renommer temporairement une colonne ou une table dans une requête
-- colonne AS alias, AS est optionel => colonne Alias 
-- Sur les colonnes
SELECT titre AS titre_livre,2023-annee AS age_livre FROM livres;
SELECT titre titre_livre,2023-annee age_livre FROM livres;

SELECT prenom, auteurs.nom AS nom_auteur, pays.nom AS pays FROM auteurs,pays;
SELECT prenom, auteurs.nom nom_auteur, pays.nom pays FROM auteurs,pays;

-- Sur les tables
SELECT prenom, a.nom , p.nom  FROM auteurs AS a,pays AS p;
SELECT prenom, a.nom , p.nom  FROM auteurs  a,pays  p;

SELECT nom FROM genres AS categorie;
SELECT nom FROM genres categorie;

-- DISTINCT -> permet d’éviter les doublons dans les résultats d’un SELECT
SELECT ALL prenom FROM auteurs;	# 36 lignes
SELECT prenom FROM auteurs; # ALL est implicite
SELECT DISTINCT prenom FROM auteurs; # 34 lignes

-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous les titres de livre qui sont sortie après 2000
SELECT titre, annee FROM livres WHERE annee>2000;
SELECT titre, 2023-annee AS age_livre FROM livres WHERE (2023-annee) <24;

-- Les opérateurs logiques AND et OR permettent de combiner des conditions
-- avec l'opérateur AND, il faut que toutes les conditions soient vrai pour que la ligne soit sélectionnée
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1980 et 1990 et qui sont des romans policier
SELECT titre,annee FROM livres WHERE annee>1980 AND annee<1990 AND genre=1;

-- avec l'opérateur OR
-- Selection de tous les titres et année de sortie de livre qui sont sortie en 1981 et en 1992
SELECT titre,annee FROM livres WHERE annee=1981 OR annee=1992;

-- Selection de tous les titres, année de sortie et genre de livre qui sont sortie après 2000 ou dont le genre est une autobiographie
SELECT titre,annee,genre FROM livres WHERE annee>2000 OR genre=2;

-- Selection de tous les titres, année de sortie et genre de livre qui sont sortie entre 1980 et 1990 ou dont le genre est une autobiographie
SELECT titre,annee,genre FROM livres WHERE (annee>1980 AND annee<1990)  OR genre=2;

-- L'opérateur NOT inverse le resultat
SELECT titre,annee,genre FROM livres WHERE NOT annee>2000;

-- L'opérateur XOR ou exclusif 
-- OR -> Sélection du noms et du prénoms  des auteurs belges ou des auteurs qui ont pour prénom Georges
SELECT prenom,nom FROM auteurs WHERE prenom='Georges' OR nation=9;
-- XOR -> Sélection du noms et du prénoms  des auteurs belges ou des auteurs qui ont pour prénom Georges mais pas des auteurs qui respect les 2 conditions
SELECT prenom,nom FROM auteurs WHERE prenom='Georges' XOR nation=9;

-- l'opérateur IN permet de vérifier, si une colonne fait partie des valeurs d'un ensemble de valeurs définis
-- Sélection du titre et de l'année pour les livres sortie en 1923,1954,1965 ou 1992
SELECT titre,annee FROM livres WHERE annee IN(1923,1954,1965,1982);

-- Sélection du prénom et du nom pour les auteurs qui ont pour prénom Pierre, James, Stephen
SELECT prenom, nom FROM auteurs WHERE prenom IN ('Pierre','James','Stephen');

-- l'opérateur BETWEEN est utilisé pour vérifier si une colonne fait partie d’un intervalle de données
-- Sélection des titres et l'année des livres qui sortie entre 1950 et 1997 
SELECT titre,annee FROM livres WHERE annee BETWEEN 1950 AND 1997; 

-- Sélection du prénom , du nom  et de la date de naissance des auteurs qui sont nés entre le 1er janvier 1930 et  le 1er janvier 1936
SELECT prenom,nom ,naissance FROM auteurs WHERE naissance BETWEEN '1930-01-01' AND '1936-01-01';

-- Sélection du prénom , du nom des auteursdont le prénom est compris entre John et Pierre
SELECT prenom,nom  FROM auteurs WHERE prenom BETWEEN 'John' AND 'Pierre';

-- L’opérateur LIKE permet d’effectuer une recherche sur un modèle particulier
--  % représente 0,1 ou plusieurs caratères inconnues
--  _ représente un caratère inconnue

-- Sélection du titre des livres qui commence par D et qui fait 4 caractères (ne tient pas compte de la casse)
SELECT titre FROM livres WHERE titre LIKE 'd___';

-- Sélection du titre des livres qui commence par D et qui fait au moins 4 caractères
SELECT titre FROM livres WHERE titre LIKE 'd___%';

-- Sélection du prénoms pour les auteurs qui ont un prénom composé
SELECT prenom, nom FROM auteurs WHERE  prenom LIKE '%-%'

-- IS NULL permet de tester si une valeur est égal à NULL 
-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui sont vivants => deces = NULL
SELECT prenom, nom,deces  FROM auteurs WHERE deces IS NULL;

-- IS NOT NULL permet de tester, si une valeur est différente de NULL
-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui sont décédés
SELECT prenom, nom,deces  FROM auteurs WHERE deces IS NOT NULL;

-- ORDER BY -> trier par ordre ascendant ASC (par défaut), ou par ordre décendant DESC

-- Selection de tous les livres triés par rapport à leur titre par ordre alphabétique
SELECT * FROM livres ORDER BY titre;

-- Sélection des titres et des années de sortie par année croissante et titre par ordre aplphabéthique décendant
SELECT titre, annee FROM livres ORDER BY annee ,titre DESC

-- Sélection des titres des livres qui sont sortie entre 1960 et 1969 triés par année croissante et titre par ordre aplphabéthique
SELECT titre, annee FROM livres WHERE annee BETWEEN 1960 AND 1969 ORDER BY annee,titre ;

-- Selection de tous les auteurs décédés trié par leur date de naissance décroissante, leur nom (croissant par défaut) et leur prénom décroissant
SELECT  prenom, nom, naissance FROM auteurs WHERE deces IS NOT NULL ORDER BY naissance DESC, nom, prenom;

-- LIMIT -> limiter le nombre de ligne du résultat

-- sélectionner les 10 auteurs les plus jeunes
SELECT prenom, nom FROM auteurs ORDER BY naissance DESC LIMIT 10;

-- sélectionner les 10 auteurs les plus jeunes à partir du 11ème
SELECT prenom, nom FROM auteurs ORDER BY naissance DESC LIMIT 10 OFFSET 10;

-- Uniquement MYSQL/ MariaDB  Syntaxe différente => LIMIT offset, limit
SELECT prenom, nom FROM auteurs ORDER BY naissance DESC LIMIT 10 OFFSET 20;
SELECT prenom, nom FROM auteurs ORDER BY naissance DESC LIMIT 20,10;
