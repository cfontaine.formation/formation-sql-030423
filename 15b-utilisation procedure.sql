USE bibliotheque;

-- appel de la procedure stocké
CALL auteur_vivant(); 

SHOW PROCEDURE STATUS;

DROP PROCEDURE auteur_vivant ;

-- déclarer une variable globale
SET @var_global = 'test';
SELECT @var_global ;
CALL test_variable(); 
-- SELECT var_local;


CALL nom_auteur(14,@var_global);
SELECT @var_global;

CALL test_if(80,@var_global);
SELECT @var_global;

CALL test_case(5,@var_global);
SELECT @var_global;

CALL test_while(2,@var_global);
SELECT @var_global;

