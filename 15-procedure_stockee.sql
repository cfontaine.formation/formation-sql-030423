-- Déclarion d'une procédure stockée
DELIMITER $ -- CHANGE le délimiteur de fin de ligne ; -> $
CREATE PROCEDURE auteur_vivant()
BEGIN
	SELECT prenom,nom,naissance FROM auteurs WHERE deces IS NULL;
END $
DELIMITER ; -- $-> ;


DELIMITER $
CREATE PROCEDURE test_variable()
BEGIN
	DECLARE var_local INT DEFAULT 10;
	SELECT var_local;
	SELECT @var_global;
END $
DELIMITER ; 

DELIMITER $
CREATE PROCEDURE nom_auteur(IN id_auteur INT,OUT nom_sortie VARCHAR(100))
BEGIN
	SELECT CONCAT(prenom,' ',nom) INTO nom_sortie FROM auteurs WHERE id=id_auteur;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE test_if(IN id_pays INT,OUT nombre_auteur INT)
BEGIN
	DECLARE nb_pays INT;
	SELECT id INTO nb_pays FROM pays WHERE id=id_pays;
	IF nb_pays IS NOT NULL THEN
		SELECT count(id) INTO nombre_auteur FROM auteurs WHERE auteurs.nation =id_pays;
	ELSE
		SET nombre_auteur=0;
	END IF;
END $
DELIMITER ;


DELIMITER $
CREATE PROCEDURE test_case(IN vi INT,OUT vo INT)
BEGIN
	CASE vi 
		WHEN 1 THEN SET vo= -42;
		WHEN 2 THEN SET vo= 42;
		ELSE SET vo= -1;
	END CASE;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE test_while(IN vi INT,OUT vo INT)
BEGIN
	DECLARE v INT DEFAULT 0;
	SET vo=0;
	WHILE v<vi DO
		SET v=v+1;
		SET vo=vo+v;
	END WHILE;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE get_auteur_by_pays(IN id_pays INT,OUT auteurs VARCHAR(1000))
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE a VARCHAR(100) ;
	DECLARE cur_auteur CURSOR FOR SELECT concat(prenom ,' ', nom) FROM auteurs WHERE auteurs.nation = id_pays; 
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done =1;	
	OPEN cur_auteur;
	SET auteurs='';
	WHILE done=0 DO
		FETCH cur_auteur INTO a;
		auteurs=auteurs+' '+ a;
	END WHILE;
	CLOSE  cur_auteur;
END $
DELIMITER ;

-- Supression de toutes les procedures stockées
DROP PROCEDURE IF EXISTS auteur_vivant ;
DROP PROCEDURE IF EXISTS test_variable ;
DROP PROCEDURE IF EXISTS nom_auteur ;
DROP PROCEDURE IF EXISTS test_if ;
DROP PROCEDURE IF EXISTS test_case ;
DROP PROCEDURE IF EXISTS test_while ;