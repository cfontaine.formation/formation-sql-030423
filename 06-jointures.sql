USE bibliotheque;

-- Jointure interne => INNER JOIN
SELECT titre, annee, livres.genre , genres.id , genres.nom FROM livres,genres
WHERE livres.genre=genres.id;

-- Relation 1,n
-- Afficher le titre, l'année de sortie et le genre du livre
SELECT titre, annee, genres.nom AS nom_genre FROM livres
INNER JOIN genres ON livres.genre = genres.id;

-- Afficher le prenom, le nom , date de naissance , la date de décès et la nationalité de l'auteur
SELECT prenom,auteurs.nom,naissance,deces,pays.nom AS nationalite FROM auteurs
INNER JOIN pays ON auteurs.nation = pays.id;

-- Relation n,m 
-- Afficher le titre du livre et le prénom et le nom de son auteurs
SELECT titre ,auteurs.prenom, auteurs.nom FROM livres
INNER JOIN livre2auteur ON livres.id = livre2auteur.id_livre
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id ORDER BY titre;

-- Afficher le prenom, le nom et la nationalité des auteurs Français ou belge
SELECT prenom, auteurs .nom, pays.nom AS nation FROM auteurs 
INNER JOIN pays ON auteurs.nation = pays.id
WHERE pays.nom IN ('France','Belgique');

-- Afficher le titre et le genre du livre et le prénom et le nom de son auteurs, pour les auteurs vivants
SELECT titre,genres.nom AS categorie,prenom, auteurs.nom,annee  FROM livres
INNER JOIN genres ON livres.genre =genres.id
INNER JOIN livre2auteur ON livres.id = livre2auteur.id_livre  
INNER JOIN auteurs ON auteurs.id= livre2auteur.id_auteur
WHERE auteurs.deces IS NULL ORDER BY livres.annee;


-- CROSS JOIN => combinaison de toutes les possbilitées
-- produit cartésien
USE exemple;
CREATE TABLE plats (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

CREATE TABLE boissons (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO plats(nom) VALUES
('céréale'),
('pain'),
('oeuf sur le plat');

INSERT INTO boissons(nom) VALUES 
('café'),
('thé'),
('jus d\' orange');

SELECT plats.nom , boissons.nom FROM plats
CROSS JOIN boissons;


USE bibliotheque;

SELECT genres.nom,titre , annee FROM genres LEFT JOIN livres ON genres.id = livres.genre 

SELECT genres.nom FROM genres
LEFT JOIN livres 
ON genres.id = livres.genre
WHERE livres.id IS NULL; 


SELECT titre ,genres.nom FROM livres RIGHT JOIN genres ON livres.genre=genres.id; 

SELECT titre ,genres.nom FROM livres FULL JOIN genres ON genre=genres.id; 

-- Jointure naturelle
ALTER TABLE genres CHANGE id genre INT;

SELECT titre, genres.nom  FROM livres NATURAL JOIN genres;

ALTER TABLE genres CHANGE genre id INT;


-- auto jointure

USE exemple;

CREATE TABLE salaries(
	id INT PRIMARY KEY  AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	manager INT
)

ALTER TABLE salaries ADD
CONSTRAINT fk_manager
FOREIGN KEY(manager)
REFERENCES salaries(id);

INSERT INTO salaries (prenom,nom,manager) VALUES 
('John','Doe',NULL),
('Alan','Smithee',1),
('Jo','Dalton',1),
('Jane','Doe',3),
('yves','roulot',3);

SELECT 	employe.prenom AS employe_prenom, employe.nom AS employe_nom,
		manager.prenom AS manager_prenom, manager.nom AS manager_nom
FROM salaries AS employe
LEFT JOIN salaries AS manager
ON employe.manager=manager.id;
