USE bibliotheque;

-- requete imbriqué qui ne retourne qu'un seul resultat
SELECT titre,annee 
FROM livres WHERE annee=(
	SELECT round(AVG(annee),0) FROM livres
);

SELECT titre,annee FROM livres 
WHERE annee=(
	SELECT annee FROM livres ORDER BY annee DESC LIMIT 1
);

-- requete imbriqué qui retourne plusieurs resultats

-- IN
SELECT titre, annee ,genre FROM livres 
WHERE annee IN (
	SELECT annee FROM livres
	INNER JOIN genres
	ON livres.genre = genres.id 
	WHERE genres.nom='Horreur'
);

-- EXISTS 
SELECT genres.id, genres.nom FROM genres WHERE 
EXISTS(
	SELECT livres.id FROM livres WHERE livres.genre = genres.id
);

-- NOT EXISTS
SELECT * FROM genres WHERE 
NOT EXISTS(
	SELECT livres.id FROM Livres WHERE livres.genre = genres.id
);

-- ALL 
SELECT titre, annee FROM livres 
WHERE annee <> ALL(
	SELECT annee FROM livres
	INNER JOIN genres
	ON livres.genre = genres.id 
	WHERE genres.nom='Science-fiction'
);

-- ANY ou SOME
SELECT titre, annee,genre FROM livres 
WHERE annee = ANY(
	SELECT annee FROM livres
	INNER JOIN genres
	ON livres.genre = genres.id 
	WHERE genres.nom='Science-fiction'
);

SELECT titre, annee,genre FROM livres 
WHERE annee = ANY(
	SELECT annee FROM livres
	INNER JOIN genres
	ON livres.genre = genres.id 
	WHERE genres.nom='Science-fiction'
) OR annee = ANY(
	SELECT annee FROM livres
	INNER JOIN genres
	ON livres.genre = genres.id 
	WHERE genres.nom='Autobiographie'
);

SELECT titre, annee,genre FROM livres # Les livres qui sont la même année qu'un livre policier
WHERE annee = ANY(
	SELECT annee FROM livres # liste des années où un livre policier est sortie
	INNER JOIN genres
	ON livres.genre = genres.id 
	WHERE genres.nom=( # policier
		SELECT genres.nom FROM livres
		INNER JOIN genres
		ON livres.genre = genres.id
		GROUP BY  genres.id 
		ORDER BY count(livres.titre) DESC
		LIMIT 1
	)
) ;

-- Sous requête exercice
USE elevage;
-- Afficher la liste des noms animaux qui sont de l'espèce la plus représentée dans l'élevage
SELECT nom FROM Animal
WHERE espece_id = ( 
   	SELECT espece_id  FROM Animal
	GROUP BY espece_id 
	ORDER BY count(espece_id) DESC
	LIMIT 1
   );
  
-- Afficher la liste des noms animaux pour toutes les espèces dont le nom commence par 'tor'
SELECT * FROM Animal
WHERE espece_id = ( 
	 SELECT id FROM Espece WHERE nom_courant LIKE 'tor%'
);

-- Afficher le nom de la race pour lequel on a des animaux dans l'élevage
SELECT * FROM Race
WHERE EXISTS (SELECT * FROM Animal WHERE Animal.race_id = Race.id);
   
-- Afficher la liste des noms animaux qui font partie des 3 races les plus représenté
SELECT count(animal.id), race.nom FROM race 
INNER JOIN animal ON race.id=animal.race_id 
GROUP BY race.id
ORDER BY count(animal.id) DESC;
-- Afficher le nom de la race pour lequel on a des animaux dans l'élevage
   

    
