USE bibliotheque;

SELECT count(auteurs.id) nombre_auteur, pays.nom  nation FROM auteurs
INNER JOIN pays
ON pays.id=auteurs.nation
GROUP BY auteurs.nation
ORDER BY count(auteurs.id) DESC;

SELECT count(livres.id), round(avg(annee),0), genres.nom FROM livres
INNER JOIN genres
ON genres.id = livres.genre  
GROUP BY genre;

SELECT count(livres.id), genres.nom FROM livres
INNER JOIN genres
ON genres.id = livres.genre  
WHERE annee>1960
GROUP BY genre
HAVING count(livres.id)>4;

SELECT CONCAT_WS(' ',auteurs.prenom, auteurs.nom) AS auteur,count(titre) AS nombre_livre
FROM auteurs
INNER JOIN livre2auteur
ON auteurs.id = livre2auteur.id_auteur 
INNER JOIN livres
ON livres.id=livre2auteur.id_livre
WHERE deces IS NOT NULL
GROUP BY auteur
HAVING  nombre_livre>4
ORDER BY nombre_livre DESC
LIMIT 5;

USE world;
-- Exercice regroupement (agrégation)
-- Nombre de pays par continent
SELECT continent,count(code) nbpays
  FROM country
 GROUP BY continent
 ORDER BY nbpays;
 
-- Afficher les continents et leur la population totale classé du plus peuplé au moins peuplé
SELECT continent , sum(population) AS population_continent
FROM country 
GROUP BY continent
ORDER BY population_continent DESC;

-- Nombre de langue officielle par pays - classé par nombre de langue officielle
SELECT country.name,count(country_language.languages) AS nombre_langue_officielle FROM country
INNER JOIN country_language 
ON country.code = country_language.country_code 
WHERE country_language.is_official = 'T'
GROUP BY country.code
ORDER BY nombre_langue_officielle;

USE elevage;

-- Quelles sont les races dont nous ne possédons aucun individu ?
SELECT Race.nom, COUNT(Animal.race_id) AS nombre
FROM Race
LEFT JOIN Animal ON Animal.race_id = Race.id
GROUP BY Race.nom
HAVING nombre = 0;

-- ROLLUP 
USE Exemple;

SELECT temps_travail_semaine , sum(salaire_mensuel) AS total FROM employees
GROUP BY temps_travail_semaine WITH ROLLUP ;

USE elevage;
-- Combien de mâles et de femelles de chaque race avons-nous, avec un compte total intermédiaire pour les races (mâles et femelles confondues) et pour les espèces ?
-- Afficher le nom de la race et le nom courant de l'espèce
SELECT Animal.sexe, Race.nom, Espece.nom_courant, COUNT(*) AS nombre
FROM Animal
INNER JOIN Espece ON Animal.espece_id = Espece.id
INNER JOIN Race ON Animal.race_id = Race.id
WHERE Animal.sexe IS NOT NULL
GROUP BY Espece.nom_courant, Race.nom, sexe WITH ROLLUP;

USE exemple;

CREATE TABLE ventes(
	id INT PRIMARY KEY AUTO_INCREMENT, 
	nom_vendeur VARCHAR(40),
	annee INT,
	vente DECIMAL(14,2)
);

INSERT INTO ventes(nom_vendeur,annee,vente)
VALUES 
('Paul',2020,10000),
('Patrick',2020,11000),
('Paola',2020,15000),
('Paul',2021,8000),
('Patrick',2021,10000),
('Paola',2021,5000),
('Paul',2022,14000),
('Patrick',2022,5000),
('Paola',2022,16000);

INSERT INTO ventes(nom_vendeur,annee,vente)
VALUES 
('Paul',2019,12000),
('Patrick',2019,10000),
('Paola',2019,4000),
('Paul',2018,8000),
('Patrick',2018,10000),
('Paola',2018,5000),
('Paul',2017,1000),
('Patrick',2017,5000),
('Paola',2017,6000);

SELECT sum(vente) FROM ventes;

SELECT annee,sum(vente) AS total_annee FROM ventes GROUP BY annee;

SELECT annee, nom_vendeur , vente, 
sum(vente) OVER (PARTITION BY annee
ORDER BY annee ) AS total_annee 
FROM ventes;

-- DENSE_RANK

SELECT annee, nom_vendeur , vente,
DENSE_RANK() OVER(PARTITION BY annee
ORDER BY vente DESC
) AS Classement_vente 
FROM ventes;

-- CUME_DIST
SELECT annee, nom_vendeur , vente,
CUME_DIST() OVER(
ORDER BY vente DESC
) AS cume_dist,
ROW_NUMBER() OVER (ORDER BY vente DESC
) AS row_num
FROM ventes;

-- FIRST_VALUE
SELECT annee, nom_vendeur , vente,
FIRST_VALUE(nom_vendeur) OVER(
PARTITION BY annee
ORDER BY vente DESC
) AS vente_max
FROM ventes;

-- NTH_VALUE
SELECT annee, nom_vendeur , vente,
NTH_VALUE(nom_vendeur,3) OVER(
ORDER BY vente DESC
) AS vente_max
FROM ventes;

-- PERCENT_RANK
SELECT annee, nom_vendeur , vente,100.0*
PERCENT_RANK() OVER(
ORDER BY vente DESC
) AS vente_max
FROM ventes;


SELECT annee, nom_vendeur , vente, 
sum(vente) OVER (
ORDER BY annee 
ROWS BETWEEN  2 PRECEDING  AND 3  FOLLOWING) AS total_annee 
FROM ventes;