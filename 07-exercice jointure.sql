USE world;

-- Afficher chaque pays et sa capitale
SELECT country.name AS pays, city.name AS capitale FROM country
INNER JOIN city ON city.id= country.capital;

-- Afficher les nom des pays et le nom de ses villes classer par nom de pays puis par nom de ville en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name AS pays, city.name AS ville FROM country
INNER JOIN city ON country.code = city.country_code
ORDER BY country.name,city.name;

-- Afficher: les noms de  pays, la  langue et le  pourcentage  classé par pays et par  pourcentage décroissant
SELECT country.name AS pays, country_language.languages  AS langue , country_language.percentage AS pourcentage 
FROM country
INNER JOIN country_language ON country.code=country_language.country_code
ORDER BY country.name,country_language.percentage DESC;

-- Afficher le nom des pays sans ville
SELECT country.name AS pays 
FROM country
LEFT JOIN city
ON country.code = city.country_code
WHERE country_code IS NULL;

-- Afficher le  nom du pays, sa langue officielle (sans les pays qui n'ont pas de langue officielle) 
SELECT country.name, country_language.languages 
FROM country
INNER JOIN country_language 
ON country.code = country_language.country_code
WHERE country_language.is_official = 'T'
ORDER BY country.name
; 

-- Afficher chaque ville et son pays
SELECT city.name ,country.name 
FROM country
INNER JOIN city
ON city.country_code =country.code; 

-- Afficher tous les pays qui parlent français
SELECT country.name AS pays_francophone 
FROM country 
LEFT JOIN country_language
ON country.code =country_language.country_code
WHERE country_language.languages='French';