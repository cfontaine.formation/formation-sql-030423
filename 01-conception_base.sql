/*01 -Conception de base */

-- Supprimer la base exemple, si elle existe 
DROP DATABASE IF EXISTS exemple;

-- Créer une base de donnée
CREATE DATABASE exemple;

-- Choisir la base de donnée exemple, comme base courante
USE exemple; 

-- Afficher la liste des bases de données
SHOW DATABASES;

-- Créer une table articles
CREATE TABLE article(
	reference INT,
	description VARCHAR(255) DEFAULT 'un article',
	prix DOUBLE NOT NULL DEFAULT 10.0
);

-- Effacer une table 
-- DROP TABLE article;

-- AFFICHER la liste des tables de la bbd
SHOW TABLES;

-- Afficher la description de la table
DESCRIBE articles;

-- Exercice: créer une table vols
CREATE TABLE vols(
	numero_vol INT,
	heure_depart DATETIME,
	ville_depart VARCHAR(100),
	heure_arrivee DATETIME,
	ville_arrivee VARCHAR(100)
);

-- Ajouter la clé primaire dans la table articles 
ALTER TABLE articles 
ADD CONSTRAINT PK_articles PRIMARY KEY(reference);

-- Création de la table marques
CREATE TABLE marques(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30),
	date_creation DATE
);

-- Renomer une table 
RENAME TABLE article TO articles;

-- Ajouter une colonne à la table articles
ALTER TABLE articles ADD id_marque INT;

-- Ajouter une clef etrangères
-- Relation 1,n
-- un article a une marque , une marque a plusieurs article
ALTER TABLE articles ADD 
CONSTRAINT fk_articles_marques
FOREIGN KEY (id_marque)
REFERENCES marques(id); 

-- Ajouter une colonne à la table articles
ALTER TABLE marques ADD nationalite VARCHAR(30);

-- Suprimer une colonne
ALTER TABLE marques DROP nationalite;

-- Changer le type d'une colonne double -> decimal
ALTER TABLE articles MODIFY prix DECIMAL(6,2);

-- Renommer la colonne prix -> prix_unitaire
ALTER TABLE articles CHANGE prix prix_unitaire DECIMAL(6,2);

-- Clé étrangère
-- relation n -n
-- un fournisseurs fournit plusieurs articles 
-- un articles est fournit par plusieurs fournisseurs 
CREATE TABLE fournisseurs
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(40)
); 

-- Création de la table de jonction
CREATE TABLE articles_fournisseurs(
	reference_article INT,
	id_fournisseur INT,
	
	-- La clé primaire est composée des 2 colonnes reference_article, id_fournisseur
	CONSTRAINT PK_articles_forunisseurs
	PRIMARY KEY(reference_article,id_fournisseur),
	
	-- reference_article -> Clé étrangère 
	CONSTRAINT FK_articles 
	FOREIGN KEY (reference_article)
	REFERENCES articles(reference),
	
	-- id_fournisseur -> Clé étrangère
	CONSTRAINT FK_fournisseurs
	FOREIGN KEY (id_fournisseur)
	REFERENCES fournisseurs (id)
	);

-- Ajout d'une clé primaire (numero_vol) à la table vols
ALTER TABLE vols ADD 
CONSTRAINT PK_vols PRIMARY KEY (numero_vol);

-- Ajout d'un colonne à la table vols (clé étrangères vers la tables pilotes)
ALTER TABLE vols ADD id_pilote INT;

-- Création de la table pilotes
CREATE TABLE pilotes (
	numero_pilote INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40) NOT NULL,
	nom VARCHAR(40) NOT NULL,
	nombre_heure_vol INT NOT NULL
);

-- Ajout de clé étrangères entre vols et pilotes
ALTER TABLE vols ADD
CONSTRAINT FK_vols_pilotes
FOREIGN KEY (id_pilote)
REFERENCES pilotes (numero_pilote);

-- Ajout d'un colonne à la table vols (clé étrangères vers la tables avions)
ALTER TABLE vols ADD id_avions INT;

-- Création de la table avions
CREATE TABLE avions (
	numero_avion INT PRIMARY KEY AUTO_INCREMENT,
	modele VARCHAR(60) NOT NULL,
	capacite INT NOT NULL
);

-- Ajout de clé étrangères entre vols et pilotes
ALTER TABLE vols ADD
CONSTRAINT FK_vols_avions
FOREIGN KEY (id_avions)
REFERENCES avions (numero_avion);

-- Modifier le type de la colonne capacite  INT -> SMALLINT
ALTER TABLE avions MODIFY capacite SMALLINT; 


-- Contrainte CHECK
CREATE TABLE logements(
	id INT PRIMARY KEY AUTO_INCREMENT, 
	adresse VARCHAR(200),
	nombre_piece INT CHECK(nombre_piece>2),
	loyer DOUBLE CHECK(loyer>0),
	surface INT CHECK(surface>9)
);

INSERT INTO logements(adresse,nombre_piece ,loyer,surface) VALUES
('1, Rue Solferino LILLE 59000',4,500,50),
('32, Avenue 4,septembre LENS 62300',5,900,100);

-- Ne respecte pas les contrainte
-- INSERT INTO logements(adresse,nombre_piece ,loyer,surface) VALUES
-- ('100, Rue Solferino ROUBAIX',1,200,10);
-- 
-- INSERT INTO logements(adresse,nombre_piece ,loyer,surface) VALUES
-- ('102, Rue Solferino CROIX',3,-1200,100);
-- 
-- INSERT INTO logements(adresse,nombre_piece ,loyer,surface) VALUES
-- ('101, Rue Solferino CROIX',2,200,8);

ALTER TABLE articles ADD 
CONSTRAINT CHK_articles
CHECK (prix_unitaire >0 AND char_length( description)>4  );

-- INSERT INTO articles (reference,description,prix_unitaire,id_marque)
-- VALUES(3000,'Adaptateur USB/RJ45',-29.00,1);

INSERT INTO articles (reference,description,prix_unitaire,id_marque)
VALUES(3001,'K30',119.00,1);

ALTER TABLE marques ADD
CONSTRAINT CHK_marques
CHECK (date_creation<='2023-04-01');
INSERT INTO marques(nom,date_creation)
VALUES ('Marque e','1923-01-15');

-- INSERT INTO marques(nom,date_creation)
-- VALUES ('Marque f','2023-09-15');

