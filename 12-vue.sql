USE bibliotheque;
-- Les vues
CREATE VIEW v_auteur_livre AS 
	SELECT livres.id, livres.titre, livres.annee, 
	concat(auteurs.prenom,' ',auteurs.nom) AS auteur, genres.nom
	FROM livres
	INNER JOIN livre2auteur
	ON livres.id= livre2auteur.id_livre
	INNER JOIN auteurs
	ON auteurs.id=livre2auteur.id_auteur 
	INNER JOIN genres
	ON livres.genre=genres.id;

SELECT * FROM v_auteur_livre
WHERE annee>2000;

ALTER VIEW v_auteur_livre AS 
SELECT livres.id, livres.titre, livres.annee, 
	concat(auteurs.prenom,' ',auteurs.nom) AS auteur,
	genres.nom AS genre
	FROM livres
	INNER JOIN livre2auteur
	ON livres.id= livre2auteur.id_livre
	INNER JOIN auteurs
	ON auteurs.id=livre2auteur.id_auteur 
	INNER JOIN genres
	ON livres.genre=genres.id;

SHOW Tables;

DROP VIEW v_auteur_livre ;