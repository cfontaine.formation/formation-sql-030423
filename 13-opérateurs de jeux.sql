USE exemple

CREATE TABLE employees(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_personnel VARCHAR(255),
	salaire_mensuel double,
	temps_travail_semaine INT
);


CREATE TABLE clients(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_livraison VARCHAR(255),
	adresse_personnel VARCHAR(255)
);

INSERT INTO employees(prenom,nom, adresse_personnel,salaire_mensuel,temps_travail_semaine)
VALUES
('John','Doe','1,rue esquermoise Lille',1800,35),
('Jane','Doe','1,rue esquermoise Lille',2000,35),
('Jo','Dalton','46,rue des Cannoniers  Lille',2800,40),
('Alan','Smithee','32 Boulevard Vincent Gache Nantes',2300,37),
('Yves','Roulo','1,46,rue des Cannoniers  Lille',1400,20);

INSERT INTO Clients(prenom,nom,adresse_livraison , adresse_personnel)
VALUES
('John','Doe','1,rue esquermoise Lille','1,rue esquermoise Lille'),
('Pierre','Martin','23,rue esquermoise Lille','23,rue esquermoise Lille'),
('Bastien','Dupond','46,rue des Cannoniers  Lille','46,rue des Cannoniers  Lille');

SELECT prenom,nom,adresse_personnel, 'emp' AS identifiant  FROM employees
UNION
SELECT prenom,nom,adresse_personnel,  'cl'  FROM clients;

SELECT prenom,nom,adresse_personnel,  'emp' AS identifiant FROM employees
UNION ALL
SELECT prenom,nom,adresse_personnel , 'cl'  FROM clients;

SELECT prenom,nom,adresse_personnel FROM employees
INTERSECT 
SELECT prenom,nom,adresse_personnel FROM clients;

SELECT prenom,nom,adresse_personnel FROM employees
EXCEPT 
SELECT prenom,nom,adresse_personnel FROM clients;

-- Excercices Opérateurs de jeux
USE world;

-- Liste pays et ville ayant moins de 1000 habitant
SELECT name, 'ville' as type , population
FROM city
WHERE population < 1000
UNION
SELECT name, 'pays' as type , population
FROM country
WHERE population < 1000

-- Liste des pays où on parle français ou anglais
SELECT name as nom, 'Français' AS langue
FROM country_language
INNER JOIN country ON code = country_code
WHERE languages = 'French'
UNION
SELECT name , 'Anglais' as nom_pays
FROM country_language
INNER JOIN country ON code = country_code
WHERE languages = 'English'
ORDER BY Nom;
