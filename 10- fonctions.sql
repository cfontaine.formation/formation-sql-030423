USE bibliotheque;
-- Syntaxe d'une fonction
-- nom_fonction(param1,param2 ...)

-- Fonctions arithmétiques
SELECT ceil(1.4) ceil , floor(1.4) floor,
round (1.49567878,4) round,
truncate(1.49567878,4) truncate, rand();

SELECT titre FROM livres ORDER BY rand() LIMIT 5;

-- Fonction chaine de caractères
SELECT nom , length(nom) AS nombre_octet FROM auteurs;

SELECT prenom,nom ,char_length(nom) AS nombre_caractere FROM auteurs WHERE char_length(nom)<6; 

SELECT ascii('a'), ascii('A');

SELECT concat (prenom,' ',auteurs.nom, ' (',pays.nom ,')') AS auteur FROM auteurs
INNER JOIN pays ON pays.id=auteurs.nation ;

SELECT concat_ws ('  ',prenom,nom,naissance) AS auteur FROM auteurs;

SELECT concat ('|',space(15),'|') 

SELECT prenom, insert(prenom,3,2,'----')FROM auteurs;

SELECT nom,replace(nom,'er','___') FROM auteurs;

SELECT repeat(nom,4) FROM auteurs;

SELECT reverse(nom) FROM auteurs;

SELECT prenom,left(prenom,3), right(prenom,2)  FROM auteurs;

SELECT prenom,substr(prenom,2,3) FROM auteurs;

SELECT position('o' IN 'hello world'); # 5

SELECT FIND_IN_SET ('zse','aze,uio,zse,123,rty');

SELECT FIELD ('zse','aze','uio','zse','123','rty');

SELECT TRIM('          Bonjour         ');

SELECT LTRIM(' Bonjour         '); # Bonjour  
SELECT RTRIM(' Bonjour         '); # Bonjour  

SELECT lower(prenom),upper(nom) FROM auteurs;

SELECT  strcmp('bonjour','hello'), strcmp('hello','bonjour'),strcmp('hello','hello');
SELECT format(annee,3) FROM livres;

-- fonction temporelle
SELECT current_date(),current_time(),current_timestamp(),now();  

-- Date
SELECT DATE(current_timestamp()),Day(current_timestamp()) ;

SELECT day(current_timestamp()),dayname(current_timestamp()), MONTH(current_timestamp()),monthname (current_timestamp());
SELECT dayofweek(current_timestamp()),weekday(current_timestamp()) ;
SELECT week(current_timestamp(),0),week(current_timestamp(),1) ;

SELECT YEAR('1967-10-10'),quarter(current_date()) ;

SELECT prenom,nom,YEAR(naissance) FROM auteurs;

SELECT YEARWEEK(current_date()), 
INSERT(YEARWEEK(current_date()),5,0,'_');

SELECT DATEDIFF('2023-07-14',current_date());
SELECT adddate(current_date(),INTERVAL 2 month) ,subdate(current_date(),INTERVAL 4 year);

SELECT FROM_DAYS(560000),last_day('2023-04-06') ;

SELECT DATE_FORMAT(current_date(),'%e %M %Y');

SELECT STR_TO_DATE('10/05/2023','%d/%m/%Y');

-- L'heure
SELECT HOUR(current_time()),MINUTE(current_time()),SECOND(current_time()),microsecond(current_time()) ;
SELECT timediff('12:30:00',current_time()),subtime(current_time(),'05:00:00'),addtime(current_time(),'05:00:00');

SELECT time_to_sec(current_time()),SEC_TO_Time(40000);
SELECT TIME_FORMAT(current_time(),'%l:%i:%s %p');

-- Fonction d'agrégation

SELECT count(id) ,min(annee), max(annee),truncate(avg(annee),0) FROM livres;

-- Nombre de pays présents dans la table Country
SELECT count(id) FROM country;

-- Écrire une requête pour générer un code qui a pour forme les 3 première lettres de la ville concaténé avec la chaine '0000'et le nombre de caractère de la ville et séparé par -
SELECT CONCAT_WS('-',LEFT(name,3),'0000',char_length(name));

USE elevage;

-- Afficher la liste des animaux nés en 2006
SELECT nom,date_naissance FROM animal WHERE  YEAR(date_naissance)=2006;

-- Afficher la liste des animaux nés en 2009
SELECT nom,date_naissance FROM animal WHERE  YEAR(date_naissance)=2009 AND Mounth(date_naissance)=5 ;

-- Afficher les chats dont la deuxième lettre du nom est un a
SELECT animal.nom, espece.nom_courant  FROM animal
INNER JOIN espece ON animal.espece_id =espece.id
WHERE espece.nom_courant ='chat'AND POSITION('a' IN animal.nom)=2;

USE bibliotheque;

-- Autre fonction 
SELECT current_user(), DATABASE(),VERSION();

SELECT COALESCE(NULL,NULL,4,NULL,'az'); # 4
SELECT NULLIF('az','er'),NULLIF ('az','az');

SELECT annee,NULLIF(annee,1954) FROM livres;

SELECT annee,if(annee>2000,"21ème siècle","siècle passé") AS Siecle FROM livres;

SELECT annee,
CASE 
	WHEN annee>2000 THEN 'Moderne'
	WHEN annee<2000 AND annee>1900 THEN '20 siècle'
	ELSE 'Livre ancien'
END AS type_livre
FROM livres;

SELECT
genre, 
CASE(genre)
	WHEN 1 THEN 'Roman policier'
	WHEN 3 THEN 'Horreur'
	WHEN 7 THEN 'Drame'
	ELSE 'un autre genre'
END AS genre_livre
FROM livres;
END
